import datetime
import pickle

from flask import request, jsonify, Blueprint, session
from flask import current_app

from surongdan.extensions import db
from surongdan.models import project_table, layer_table, project_public_table, user_table, project_attitude_table, \
    project_comment_table

hub_bp = Blueprint('hub', __name__)


# 浏览创意接口：/hub/get_list
@hub_bp.route('/get_list', methods={'GET'})
def get_list():
    # 用户是否登录的检查 #
    if not session.get('logged_in'):
        return jsonify({'fault': '你还没有登录'}), 403
    project_list = project_public_table.query.all()
    plst = []
    for proj in project_list:
        p = project_table.query.get(proj.project_id)
        u = user_table.query.get(proj.project_user_id)
        pa = project_attitude_table.query.get((proj.project_id, session.get('user_id')))
        if pa is None:
            is_like=False
            is_hate=False
        else:
            is_like=pa.project_is_like
            is_hate=pa.project_is_hate
        plst.append({"project_id": p.project_id,
                     "project_name": p.project_name,
                     "project_desc": p.project_info,
                     "project_user_name": u.user_name,
                     "project_image": p.project_image,
                     "project_like_num": proj.project_like_num,
                     "project_is_like": is_like,
                     "project_hate_num": proj.project_hate_num,
                     "project_is_hate": is_hate
                     })
    return jsonify({"plst": plst}), 200


# 发布创意接口：/hub/set_public
@hub_bp.route('/set_public', methods={'POST'})
def set_public():
    data = request.get_json()
    # print(data)
    try:
        pid = int(data['project_id'])
    except:
        return jsonify({'fault': '项目id出错'}), 400
    # 数据正确性检查
    if data.get('project_id') is None:
        return jsonify({'fault': '数据错误, 需要project_id'}), 400
    
    # 用户是否登录的检查 #
    if not session.get('logged_in'):
        return jsonify({'fault': '你还没有登录'}), 403
    public_p = project_public_table.query.get(int(data['project_id']))
    # 检查项目是否公开
    if public_p:
        return jsonify({'msg': '成功设为公有'}), 200
        # return jsonify({'fault': 'project is already public'}), 400
    p = project_table.query.get(int(data['project_id']))
    # 检查项目是否存在
    if p is None:
        return jsonify({'fault': '项目不存在'}), 404
    # 用户是否是被复制项目拥有者的检查
    if session.get('user_id') != p.project_user_id:
        return jsonify({'fault': '用户不是项目拥有者'}), 403

    # 对新项进行赋值
    public_p = project_public_table(project_id=p.project_id,
                                    project_user_id=p.project_user_id,
                                    project_like_num=0,
                                    project_hate_num=0)
    with db.auto_commit_db():
        db.session.add(public_p)
    return jsonify({'msg': '成功设为公有'}), 200


# 删除创意接口：/hub/set_private
@hub_bp.route('/set_private', methods={'POST'})
def set_private():
    data = request.get_json()
    # print(data)
    # 数据正确性检查
    if data.get('project_id') is None:
        return jsonify({'fault': '数据错误, 需要project_id'}), 400
    # 用户是否登录的检查 #
    if not session.get('logged_in'):
        return jsonify({'fault': '你还没有登录'}), 403
    public_p = project_public_table.query.get(int(data['project_id']))
    # 检查项目是否公开
    if public_p is None:
        return jsonify({'msg': '成功设为私有'}), 200
        # return jsonify({'fault': '项目不是公开项目'}), 400
    p = project_table.query.get(int(data['project_id']))
    # 检查项目是否存在
    if p is None:
        return jsonify({'fault': '项目不存在'}), 404
    # 用户是否是被删除项目拥有者的检查
    if session.get('user_id') != p.project_user_id:
        return jsonify({'fault': '用户不是项目拥有者'}), 403

    # 对数据库进行操作
    with db.auto_commit_db():
        db.session.delete(public_p)
    return jsonify({'msg': '成功设为私有'}), 200


# 转存工程接口：/hub/fork
@hub_bp.route('/fork', methods={'POST'})
def fork():
    config = current_app.config
    data = request.get_json()
    # print(data)
    # 数据正确性检查
    if data.get('project_id') is None:
        return jsonify({'fault': '数据错误, 需要project_id'}), 400
    if(not (str(data['project_id'])).isdigit()):
        return jsonify({'fault': '数据错误, project_id不是数字'}), 400
    # 用户是否登录的检查 #
    if not session.get('logged_in'):
        return jsonify({'fault': '你还没有登录'}), 403

    p = project_public_table.query.get(int(data['project_id']))
    # 检查项目是否公开
    if p is None:
        return jsonify({'fault': '项目不是公开项目'}), 404
    p = project_table.query.get(int(data['project_id']))
    # 检查项目是否存在
    if p is None:
        return jsonify({'fault': '项目不存在'}), 404

    # 对新项进行赋值
    new_p = project_table(project_user_id=session.get('user_id'),
                          project_name=p.project_name,
                          project_info=p.project_info,
                          project_dtime=p.project_dtime,
                          project_layer=p.project_layer,
                          project_edge=p.project_edge,
                          project_dataset_id=p.project_dataset_id,
                          project_outpath=config['SURONG_OUT_PATH'],
                          project_code_model=p.project_code_model,
                          project_code_train=p.project_code_train,
                          project_status="init",
                          project_json =p.project_json,
                          project_image =p.project_image)

    # 提交数据库，project的提交与layer一起进行，方便进行回滚,避免出现失效的project数据
    with db.auto_commit_db():
        db.session.add(new_p)
        # 依次新建所有的layer
        # layer_obj = []  # 存储layer的列表，之后一次性提交所有的数据库更改，便于回滚
        if p.project_layer is not None:
            # print(p.project_layer)
            project_layer = pickle.loads(p.project_layer)
            for i in range(len(project_layer)):
                old_layer = layer_table.query.get([project_layer[i], p.project_id])
                new_layer = layer_table(layer_id=old_layer.layer_id,
                                        layer_project_id=new_p.project_id,
                                        layer_module_id=old_layer.layer_module_id,
                                        layer_param_list=old_layer.layer_param_list
                                        )
                db.session.add(new_layer)

    p = project_table.query.get(new_p.project_id)
    p.project_outpath += ('/'+str(p.project_id))
    db.session.commit()

    return jsonify({'project_id': new_p.project_id, 'msg': 'copy success'}), 200


# 创意点赞接口：/hub/like
@hub_bp.route('/like', methods={'POST'})
def like():
    data = request.get_json()
    # print(data)
    # 数据正确性检查
    if data.get('project_id') is None:
        return jsonify({'fault': '数据错误, 需要project_id'}), 400
    # 用户是否登录的检查 #
    if not session.get('logged_in'):
        return jsonify({'fault': '你还没有登录'}), 403
    public_p = project_public_table.query.get(int(data['project_id']))
    # 检查项目是否公开
    if public_p is None:
        return jsonify({'msg': '项目不是公开项目'}), 400

    # 对新项进行赋值
    pa = project_attitude_table.query.get([int(data['project_id']), session.get('user_id')])
    if pa is None:
        pa = project_attitude_table(project_id=data['project_id'],
                                    project_user_id=session.get('user_id'),
                                    project_is_like=False,
                                    project_is_hate=False)
        with db.auto_commit_db():
            db.session.add(pa)
    with db.auto_commit_db():
        if pa.project_is_like:
            public_p.project_like_num -= 1
        else:
            public_p.project_like_num += 1
        pa.project_is_like = not pa.project_is_like
    return jsonify({'msg': 'set like success'}), 200


# 创意点踩接口：/hub/hate
@hub_bp.route('/hate', methods={'POST'})
def hate():
    data = request.get_json()
    # print(data)
    # 数据正确性检查
    if data.get('project_id') is None:
        return jsonify({'fault': '数据错误, 需要project_id'}), 400
    # 用户是否登录的检查 #
    if not session.get('logged_in'):
        return jsonify({'fault': '你还没有登录'}), 403
    public_p = project_public_table.query.get(int(data['project_id']))
    # 检查项目是否公开
    if public_p is None:
        return jsonify({'msg': '项目不是公开项目'}), 400

    # 对新项进行赋值
    pa = project_attitude_table.query.get((int(data['project_id']), session.get('user_id')))
    if pa is None:
        pa = project_attitude_table(project_id=data['project_id'],
                                    project_user_id=session.get('user_id'),
                                    project_is_like=False,
                                    project_is_hate=False)
        with db.auto_commit_db():
            db.session.add(pa)
    with db.auto_commit_db():
        if pa.project_is_hate:
            public_p.project_hate_num -= 1
        else:
            public_p.project_hate_num += 1
        pa.project_is_hate = not pa.project_is_hate
    return jsonify({'msg': '讨厌成功'}), 200


# 评论接口：/hub/comment
@hub_bp.route('/comment', methods={'POST'})
def comment():
    data = request.get_json()
    # print(data)
    # 数据正确性检查
    if data.get('project_id') is None:
        return jsonify({'fault': '数据错误, 需要project_id'}), 400
    if data.get('comment') is None:
        return jsonify({'fault': '数据错误, 需要comment'}), 400
    # 用户是否登录的检查 #
    if not session.get('logged_in'):
        return jsonify({'fault': '你还没有登录'}), 403
    public_p = project_public_table.query.get(int(data['project_id']))
    # 检查项目是否公开
    if public_p is None:
        return jsonify({'msg': '项目不是公开项目'}), 400

    # 对新项进行赋值
    c = project_comment_table(project_id=data['project_id'],
                              project_user_id=session.get('user_id'),
                              comment_dtime=datetime.datetime.now(),
                              comment=data['comment'])
    with db.auto_commit_db():
        db.session.add(c)
    return jsonify({'msg': '评论成功'}), 200


# 浏览评论接口：/hub/get_comment
@hub_bp.route('/get_comment', methods={'GET'})
def get_comment():
    project_id=request.args.get("project_id")
    # 数据正确性检查
    if project_id is None:
        return jsonify({'fault': '数据错误, 需要project_id'}), 400
    # 用户是否登录的检查 #
    if not session.get('logged_in'):
        return jsonify({'fault': '你还没有登录'}), 403
    # comment_list = project_comment_table.query.get(int(data['project_id'])).all()
    comment_list = project_comment_table.query.with_entities(project_comment_table.project_user_id,
                                                             project_comment_table.comment_dtime,
                                                             project_comment_table.comment, ).filter(
        project_comment_table.project_id == int(project_id)).all()
    project_comments = []
    for c in comment_list:
        u = user_table.query.get(c.project_user_id)
        project_comments.append({"user_name": u.user_name,
                                 "comment_dtime": c.comment_dtime,
                                 "comment": c.comment,
                                 })
    return jsonify({"project_comments": project_comments}), 200
