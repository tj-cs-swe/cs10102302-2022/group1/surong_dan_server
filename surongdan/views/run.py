import datetime
import pickle
from flask import current_app
from flask import request, jsonify, Blueprint, session
from flask import send_file
from sqlalchemy import and_
# from concurrent.futures import ThreadPoolExecutor
from flask_executor import Executor
from surongdan.models import project_table, layer_table, dataset_table, project_superparam_table
from surongdan.precode import *
from surongdan.gencode import *
from surongdan.runcode import *

run_bp = Blueprint('run', __name__)

@run_bp.route('/test_gen_code', methods={'GET'})
def test_gen_code():
    if gen_code(3):
        return jsonify({'msg':'ok'}), 201
    else:
        return jsonify({'fault':'false'}), 500

# 获取数据集
@run_bp.route('/get_dataset_list', methods={'GET'})
def get_dataset_list():
    # 用户是否登录的检查
    user_id = session.get('user_id')
    if user_id is None:
        return jsonify({'fault': '未登录，请先登录.'}), 403
    # 查询所有可以使用的数据集
    dataset_objs = dataset_table.query.all()
    dataset_lst = []
    for dataset in dataset_objs:
        dataset_lst.append({'dataset_id':dataset.dataset_id, 
                            'dataset_name':dataset.dataset_name, 
                            'dataset_desc':dataset.dataset_desc})
    return jsonify(dataset_lst), 200

# 获取代码
@run_bp.route('/get_code/<project_id>', methods={'GET'})
def get_code(project_id):
    # 用户是否登录的检查
    user_id = session.get('user_id')
    if user_id is None:
        return jsonify({'fault': '未登录，请先登录.'}), 403
    
    # 进行数据正确性检查
    if project_id is None:
        return jsonify({'fault': '缺少项目id.'}), 400
    if not str(project_id).isdigit():
        return jsonify({'fault': '数据有误，请检查.'}), 400
    if int(project_id) < 0:
        return jsonify({'fault': '数据有误，请检查.'}), 400

    # 查询项目
    p = project_table.query.get(int(project_id))
    if p is None:
        return jsonify({'fault':'对不起，项目不存在.'}), 404
    # 代码已生成，直接返回
    if p.project_status == 'ready' or p.project_status == 'done' or p.project_status=='done_succ' or p.project_status=='done_fail' or p.project_status=='running':
        return jsonify({'msg':'ok', 
                        'model_code': p.project_code_model,
                        'train_code': p.project_code_train, 
                        'environment': 'Pytorch',
                        'path':'/run/urlforcode/'+project_id})
    # 需要生成代码
    # 检查是否submit_runinfo 
    if p.project_superparam_id is None:
        return jsonify({'fault':'请先提交超参数.'}), 404
    if p.project_dataset_id is None:
        return jsonify({'fault':'请先选择数据集.'}), 404
    # 检查项目结构是否为空
    if p.project_layer is None:
        return jsonify({'fault':'这是一个空项目'}), 400
    try:
        project_layer = pickle.loads(p.project_layer)
    except:
        return jsonify({'fault':'项目结构有误，请检查.'}), 400
    else:
        if project_layer == []:
            return jsonify({'fault':'这是一个空项目'}), 400
        # 生成代码
        gen_code(p.project_id)
        # 将代码存入数据库
        with open(p.project_outpath+'/model.py', 'r') as f:
            content = f.read()
            p.project_code_model = content
        with open(p.project_outpath+'/train.py', 'r') as f:
            content = f.read()
            p.project_code_train = content
        
        p.project_status = 'ready'
        db.session.commit()
        return jsonify({'msg': 'ok', 
                        'model_code': p.project_code_model, 
                        'train_code': p.project_code_train, 
                        'environment': 'Pytorch', 
                        'path': '/run/urlforcode/' + project_id})

# 未测试，发送get_run_def
@run_bp.route('/get_run_def', methods={"GET"})
def get_run_def():
    send_data = {}
    send_data["lossfn"] = ["CE"]
    send_data["optimizer"] = ["SGD","Adam"]
    return jsonify(send_data), 200


# 获取代码的静态资源文件
@run_bp.route('/urlforcode/<project_id>', methods={'GET'})
def urlforcode(project_id):
    # 检查用户是否登录
    user_id = session.get('user_id')
    if user_id is None:
        return jsonify({'fault': 'please login!'}), 403
    # 
    p = project_table.get(int(project_id))
    if p is None:
        return jsonify({'fault':'project does not exist'}), 404
    return send_file(p.project_outpath+'/'+project_id+'/code.py'), 200

# @run_bp.route('/get_dataset_list', methods={'POST', 'GET'})
# def get_dataset_list():
#     datas = dataset_table.query.all()
#     datalist_data = []
#     for dataset in datas:
#         datalist_data.append(
#             {'dataset_id': dataset.dataset_id,
#              'dataset_name': dataset.dataset_name,
#              'dataset_desc': dataset.dataset_desc
#              }
#         )
#     return jsonify({'dataset': datalist_data}), 200

@run_bp.route('/run_project', methods={'POST'})
def run_project():
    # 由工程id，获取工程outpath(全路径)
    # 返回值为1: 成功200，文件运行正常，输出正常
    # 返回值为2：成功200，文件运行异常，输出不全(一定有log，但图像不一定)
    # 在运行后，都返回 200 ， Code is running 
    data = request.get_json()
    try:
        current_proid = int(data['project_id'])
    except:
        return jsonify({'fault':'数据错误，请检查.'}), 400
    else:
        current_uid = session.get('user_id')
        proj_pro = project_table.query.filter(
            and_(project_table.project_id == current_proid, project_table.project_user_id == current_uid)).one_or_none()
        
        if proj_pro is None:
            return jsonify({'fault': '项目不存在，运行失败.'}), 404
        if proj_pro.project_status == "init":
            return jsonify({'fault': '项目的状态是init，还未生成代码.'}), 404
        if proj_pro.project_status == "running":
            return jsonify({'msg':'项目正在运行中.'}), 200
        
        with db.auto_commit_db():
            proj_pro.project_status = "running"

        outpath = proj_pro.project_outpath
        appnow = current_app._get_current_object()
        executor = Executor(appnow)

        def run_project_callback(future):
            #print("[Debug] future:",future.result())
            with appnow.app_context():
                p = project_table.query.get(current_proid)
                if future.result() == 0:
                    p.project_status = "done_succ"
                else:
                    p.project_status = "done_fail"
                db.session.commit()
        executor.add_default_done_callback(run_project_callback)
        executor.submit(runcode,outpath)
        return jsonify({'msg':'项目开始运行.'}), 200


@run_bp.route('/get_output', methods={"GET","POST"})
def get_output():
    data = request.get_json()
    try:
        data_project_id = int(data['project_id'])
    except:
        return jsonify({'fault':'数据有误，请检查.'}), 400
    else:
        # 开始
        current_proid = data_project_id
        current_uid = session.get('user_id')
        proj_pro = project_table.query.filter(
            and_(project_table.project_id == current_proid, project_table.project_user_id == current_uid)).one_or_none()
        if proj_pro is None:
            return jsonify({'fault': '项目不存在.'}), 404
        elif proj_pro.project_status == "init":
            return jsonify({'fault': '项目的状态是init，代码还没有被生成.'}), 404
        elif proj_pro.project_status == "ready":
            return jsonify({'fault': '项目的状态是ready，代码还没有被运行.'}), 404
            
        outpath = proj_pro.project_outpath
        print('工作路径为:', outpath)
        code,log, acc_train, acc_test, loss_train, loss_test = getoutput(outpath)
        print("[Debug] acc_train, acc_test, loss_train, loss_test:",acc_train, acc_test, loss_train, loss_test)

        if(code!="" and log!=""):
            code_status = 201
            msg_string = 'running'
            if proj_pro.project_status == "done_succ":
                proj_pro.project_status = "done"
                db.session.commit()
                code_status = 200
                msg_string = 'success'
            elif proj_pro.project_status == "done_fail":
                proj_pro.project_status = "done"
                db.session.commit()
                code_status = 203
                msg_string = 'fail'
            elif proj_pro.project_status == "done":
                code_status = 200
                msg_string = 'success'
            return jsonify({
                    'msg':msg_string,
                    'log' : log,
                    'train_acc' : acc_train,
                    'test_acc'  : acc_test,
                    'train_loss': loss_train,
                    'test_loss' : loss_test
            }), code_status
        elif(code!="" and log=="" and ((len(acc_train)!=0 and len(loss_train)!=0 and len(acc_test)!=0 and len(loss_test)!=0)==False)):
            code_status = 202
            msg_string = 'running'
            if proj_pro.project_status == "done_succ":
                proj_pro.project_status = "done"
                db.session.commit()
                code_status = 200
                msg_string = 'success'
            elif proj_pro.project_status == "done_fail":
                proj_pro.project_status = "done"
                db.session.commit()
                code_status = 203
                msg_string = 'fail'
            elif proj_pro.project_status == "done":
                code_status = 200
                msg_string = 'success'
            return jsonify({
                'msg': msg_string,
                'log' : log,
                'train_acc' : acc_train,
                'test_acc' : acc_test,
                'train_loss': loss_train,
                'test_loss': loss_test
            }), code_status
        else: # 应该不至于发生
                return jsonify({
                'msg':'fail',
                'fault': "出现错误，代码不存在."
            }), 405


@run_bp.route('/add_dataset', methods={'POST'})
def add_dataset():
    data = request.get_json()
    try:
        dataset_name = str(data['dataset_name'])
        dataset_desc = str(data['dataset_desc'])
        dataset_path = str(data['dataset_path'])
    except:
        return jsonify({'fault':'数据格式错误，请检查'}), 400
    else:
        dataset = dataset_table(dataset_name=dataset_name, 
                                dataset_desc=dataset_desc, 
                                dataset_path=dataset_path)
        db.session.add(dataset)
        db.session.commit()
        return jsonify({'msg':'数据集添加成功.'}), 201

@run_bp.route('/del_dataset', methods={'POST', 'GET'})
def del_dataset():
    data = request.get_json()
    try:
        data_dataset_id = int(data['dataset_id'])
    except:
        return jsonify({'fault': 'dataset_id有误，请检查'}), 400
    else:
        p = dataset_table.query.get(data_dataset_id)
        if p is None:
            return jsonify({'fault': '数据集不存在'}), 404
        with db.auto_commit_db():
            db.session.delete(p)
        return jsonify({'msg': 'delete success'}), 200


@run_bp.route('/submit_runinfo', methods={'POST', 'GET'})
def submit_runinfo():
    data = request.get_json()
    try:
        data_project_id = int(data['project_id'])
        data_dataset_id = int(data['dataset_id'])
        data_epoch = int(data['epoch'])
        data_learn_rate = float(data['learn_rate'])
        data_batch_size = int(data['batch_size'])
        data_optimizer = str(data['optimizer'])
        data_lossfn = str(data['lossfn'])
        if data_optimizer not in ["SGD","Adam"]:
            raise Exception('optimizer')
        if data_lossfn not in ["CE"]:
            raise Exception('lossfn')
    except:
        return jsonify({'fault': '数据有误，请检查.'}), 400
    else:
        if data_epoch <= 0:
            return jsonify({'msg': 'epoch 应该为正整数，数据有误.'}), 400
        if data_batch_size <= 0:
            return jsonify({'msg': 'batch_size 应该为正整数，数据有误.'}), 400
        if data_learn_rate <= 0:
            return jsonify({'msg': 'learning_rate 应该为正数，数据有误.'}), 400

        p = project_table.query.get(data_project_id)
        if p is None:
            return jsonify({'fault': '项目不存在'}), 400
        if p.project_status == "running":
            return jsonify({'fault': '项目正在运行, 提交超参数无效.'}), 400
        
        d = dataset_table.query.get(data_dataset_id)
        if d is None:
            return jsonify({'fault': '数据集不存在.'}), 400
        p.project_dataset_id = data_dataset_id
        db.session.commit()

        param_id = p.project_superparam_id
        if param_id is None:
            temp = project_superparam_table(superparam_epoch=data_epoch,
                                            superparam_batchsize=data_batch_size,
                                            superparam_learnrate=data_learn_rate,
                                            superparam_optim=data_optimizer,
                                            superparam_lossfn=data_lossfn)
            with db.auto_commit_db():
                db.session.add(temp)
            p.project_superparam_id = temp.project_superparam_id
            db.session.commit()
            return jsonify({'msg': 'superparam has created'}), 201
        else:
            superpara = project_superparam_table.query.get(int(param_id))
            superpara.superparam_epoch = data_epoch
            superpara.superparam_batchsize = data_batch_size
            superpara.superparam_learnrate = data_learn_rate
            superpara.superparam_optim = data_optimizer
            superpara.superparam_lossfn = data_lossfn
            db.session.commit()
            return jsonify({'msg': 'superparam has changed'}), 201
