from pickletools import optimize
import re
import copy
import pickle
from surongdan.extensions import db
from surongdan.models import module_def_table, project_table,project_superparam_table,dataset_table,layer_table
import os

global_model_class =\
"class myReshapeLinear(nn.Linear):\n\
\tdef __init__(self, in_features: int, out_features: int, bias: bool = True, device=None, dtype=None) -> None:\n\
\t\tsuper(myReshapeLinear, self).__init__(in_features, out_features, bias)\n\
\tdef forward(self, x: Tensor) -> Tensor:\n\
\t\tx = x.reshape(x.shape[0], -1)\n\
\t\treturn F.linear(x, self.weight, self.bias)\n\
"

global_import_packge = \
"import sys\n\
__stderr__ = sys.stderr\n\
import time\n\
import numpy as np\n\
from torchvision import transforms\n\
from torchvision.datasets import {0}\n\
from torch.utils.data import DataLoader\n\
import matplotlib.pyplot as plt\n\
import torch.nn as nn\n\
import torch.optim as optim\n\
from torch import Tensor\n\
import torch.nn.functional as F\n\
sys.stderr = open('{1}', 'a')\n\
fout = open('{1}','w+')\n\
loss_train_fout = open('{2}', 'w+')\n\
loss_test_fout = open('{3}', 'w+')\n\
acc_train_fout = open('{4}', 'w+')\n\
acc_test_fout = open('{5}', 'w+')\n\
"

global_batch_size = \
"train_batch_size = {0}\n\
test_batch_size = {1}\n"

global_deal_data = \
"transform = transforms.Compose([transforms.ToTensor(),transforms.Normalize([0.5],[0.5])])\n\
data_train = {0}('~/data/', train=True, transform=transform,target_transform=None, download=True)\n\
data_test  = {1}('~/data/', train=False, transform=transform,target_transform=None, download=True)\n\
train_loader = DataLoader(data_train, batch_size=train_batch_size, shuffle=True)\n\
test_loader = DataLoader(data_test,batch_size=test_batch_size,shuffle=True)\n\
"

global_train_test_code = "model = myModel()\n\
fout.write(str(model)+'\\n')\n\
fout.flush()\n\
num_epochs = {0}\n\
criterion = nn.{1}()\n\
LR = {2}\n\
optimizer = optim.{3}(model.parameters(), LR)\n\
train_losses = []\n\
train_acces = []\n\
eval_losses = []\n\
eval_acces = []\n\
fout.write(\"start training...\\n\")\n\
fout.flush()\n\
start_time = time.time()\n\
for epoch in range(num_epochs):\n\
\ttrain_loss = 0\n\
\ttrain_acc = 0\n\
\tmodel.train()\n\
\tfor img, label in train_loader:\n\
\t\tout = model(img)\n\
\t\tloss = criterion(out, label)\n\
\t\toptimizer.zero_grad()\n\
\t\tloss.backward()\n\
\t\toptimizer.step()\n\
\t\ttrain_loss += loss\n\
\t\t_, pred = out.max(1)\n\
\t\tnum_correct = (pred == label).sum().item()\n\
\t\tacc = num_correct / img.shape[0]\n\
\t\ttrain_acc += acc\n\
\ttrain_losses.append(train_loss.detach().numpy() / len(train_loader))\n\
\ttrain_acces.append(train_acc / len(train_loader))\n\
\tloss_train_fout.write('%f\\n' % (train_loss.detach().numpy() / len(train_loader)))\n\
\tloss_train_fout.flush()\n\
\tacc_train_fout.write('%f\\n' % (train_acc / len(train_loader)))\n\
\tacc_train_fout.flush()\n\
\teval_loss = 0\n\
\teval_acc = 0\n\
\tmodel.eval()\n\
\tfor img, label in test_loader:\n\
\t\tout = model(img)\n\
\t\tloss = criterion(out, label)\n\
\t\toptimizer.zero_grad()\n\
\t\tloss.backward()\n\
\t\toptimizer.step()\n\
\t\teval_loss += loss\n\
\t\t_, pred = out.max(1)\n\
\t\tnum_correct = (pred == label).sum().item()\n\
\t\tacc = num_correct / img.shape[0]\n\
\t\teval_acc += acc\n\
\teval_losses.append(eval_loss.detach().numpy() / len(test_loader))\n\
\teval_acces.append(eval_acc / len(test_loader))\n\
\tloss_test_fout.write('%f\\n' % (eval_loss.detach().numpy() / len(test_loader) ))\n\
\tloss_test_fout.flush()\n\
\tacc_test_fout.write('%f\\n' % (eval_acc / len(test_loader)))\n\
\tacc_test_fout.flush()\n\
"

global_train_test_code_print = \
"\tfout.write('epoch:{},Train Loss:{:.4f},Train Acc:{:.4f},Test Loss:{:.4f},Test Acc:{:.4f}\\n'.format(epoch, train_loss / len(train_loader),train_acc / len(train_loader),eval_loss / len(test_loader),eval_acc / len(test_loader)))\n\
\tfout.flush()\n\
\tstop_time = time.time()\n\
\tfout.write('time is:{:.4f}s\\n'.format(stop_time-start_time))\n\
fout.flush()\n\
fout.write('end training.\\n')\n\
fout.flush()\n\
"

global_loss_visualize = "plt.figure()\n\
plt.title(\"loss\")\n\
line1,=plt.plot(np.arange(len(train_losses)), train_losses )\n\
line2,=plt.plot(np.arange(len(eval_losses)), eval_losses )\n\
plt.legend(handles=[line1,line2], labels=['train loss','eval loss'])\n\
plt.savefig(\"{0}\")\n\
"

global_acc_visualize = "plt.figure()\n\
plt.title(\"acc\")\n\
line1,=plt.plot(np.arange(len(train_acces)), train_acces )\n\
line2,=plt.plot(np.arange(len(eval_acces)), eval_acces )\n\
plt.legend(handles=[line1,line2], labels=['train acc','eval acc'])\n\
plt.savefig(\"{0}\")\n\
fout.close()\n\
loss_train_fout.close()\n\
loss_test_fout.close()\n\
acc_train_fout.close()\n\
acc_test_fout.close()\n\
"

global_topo_nid_lst = []
global_layerid_id_map = {}
global_node_lst = {}

# 检查网络结构
def check_structure(project_id):
    p = project_table.query.get(project_id)
    project_layers = pickle.loads(p.project_layer)
    #print("Debug project_layers:{}", project_layers)
    global global_topo_nid_lst
    global global_layerid_id_map
    global global_node_lst
    global_topo_nid_lst = []
    global_layerid_id_map = {}
    global_node_lst = {}
    for layer_id in project_layers:
        global_node_lst[layer_id] = {'in':[], 'out':[]}
    project_edges = pickle.loads(p.project_edge)
    for edge in project_edges:
        global_node_lst[edge[0]]['out'].append(edge[1])
        global_node_lst[edge[1]]['in'].append(edge[0])
    #print("[global_node_lst] :", global_node_lst)
    open_node_lst = copy.deepcopy(global_node_lst)
    #print("[DEBUG ID ID]",id(open_node_lst), id(global_node_lst))
    #print('global_node_lst:', global_node_lst)
    # 检查是否可以拓扑排序，即是否存在环
    while len(open_node_lst):
        #print('open_node_lst:', open_node_lst)
        #print('len(open_node_lst):', len(open_node_lst))
        find = False
        for nid, nvalue in open_node_lst.items():
            if len(nvalue['in']) == 0:
                find = True
                # 加入topo队列
                global_topo_nid_lst.append(nid)
                # 修改进行map映射
                global_layerid_id_map[nid] = str(len(global_topo_nid_lst) - 1)
                # 修改 open_list
                for tmp_nid, tmp_nvalue in open_node_lst.items():
                    for inid in tmp_nvalue['in']:
                        if inid == nid:
                            tmp_nvalue['in'].remove(inid)
                del open_node_lst[nid]
                break
        if not find:
            return False
    #print("*180 [global_node_lst] :", global_node_lst)
    return True

# 生成模型代码
def gen_model_code(model_name, proj_id, dataset_name, import_code):
    # model_code = global_import_packge.format(dataset_name, log_output_path, loss_output_path, acc_output_path)
    #print("* gen_model_code [global_node_lst] :", global_node_lst)
    model_code = import_code
    model_code += global_model_class
    model_code += 'class ' + str(model_name) + '(nn.Module):\n'\
                    +'\tdef __init__(self):\n'\
                    +'\t\tsuper('+str(model_name)+', self).__init__()\n'
    #1.定义模块部分
    for layer_id in global_topo_nid_lst:
        #print("=== Debug lid, pid:",layer_id, proj_id)
        layer = layer_table.query.get((layer_id, proj_id))
        module = module_def_table.query.get(int(layer.layer_module_id))
        layer_param_list = pickle.loads(layer.layer_param_list)
        # print("[Debug gen_model_code] layer_param_list:", layer_param_list)
        module_code = module.module_def_precode
        # 对代码模板进行参数替换
        for p in layer_param_list:
            module_code = re.sub(r'\$\d+', str(p), module_code, 1)
        # 生成一个block的代码
        model_code += '\t\tself.block' + global_layerid_id_map[layer_id] + '=' + module_code + '\n'

    #2.forward部分
    model_code += '\tdef forward(self, x):\n'
    # print('global_topo_nid_lst:', global_topo_nid_lst)
    # print('global_node_lst:', global_node_lst)
    for id, layer_id in enumerate(global_topo_nid_lst):
        in_len = len(global_node_lst[layer_id]['in'])
        if in_len == 0:
            model_code += ('\t\tx' + str(id) + '=self.block' + str(id) + '(x)\n')
            continue
        elif in_len == 1:
            model_code += ('\t\tx' + str(id) + '=self.block' + str(id) 
                            + '(x' + global_layerid_id_map[global_node_lst[layer_id]['in'][0]] +')\n')
            continue
        model_code += ('\t\taddfor_x'+str(id)+'=')
        first_add = True
        for in_layer_id in global_node_lst[layer_id]['in']:
            if first_add:
                first_add = False
                model_code += ('x'+global_layerid_id_map[in_layer_id])
            else:
                model_code += ('+x'+global_layerid_id_map[in_layer_id])
        model_code += '\n'
        model_code += '\t\tx'+str(id)+'=self.block'+str(id)+'(addfor_x'+str(id)+')\n'
    
    model_code += '\t\treturn x'+global_layerid_id_map[global_topo_nid_lst[-1]] + '\n'
    
    return model_code

# 生成训练及测试代码
def gen_train_code(project_id, dataset_name, import_code):
    proj = project_table.query.get(project_id)
    proj_output = proj.project_outpath
    project_superparam_id = proj.project_superparam_id
    superparam = project_superparam_table.query.get(project_superparam_id)
    batch_size = superparam.superparam_batchsize
    epoch = superparam.superparam_epoch
    lr = superparam.superparam_learnrate
    optimizer = superparam.superparam_optim
    lossfn = "CrossEntropyLoss" if superparam.superparam_lossfn == "CE" else "BCEWithLogitsLoss"
    # batch_size = 128
    # epoch = 20
    # lr = 0.001
    # optimizer = 'Adam'
    # lossfn = 'CrossEntropyLoss()'

    file_output = open(proj_output+"/train.py","w")
    file_output.write(import_code)
    # file_output.write(global_import_packge.format(dataset_name, log_output_path, loss_output_path, acc_output_path))
    file_output.write("from model import *\n")
    file_output.write(global_batch_size.format(batch_size, batch_size))
    if dataset_name == "mnist":
        file_output.write(global_deal_data.format("mnist.MNIST", "mnist.MNIST"))
    elif dataset_name == "CIFAR10":
        file_output.write(global_deal_data.format("CIFAR10", "CIFAR10"))
    # print("[Debug] epoch,lossfn,lr,optimizer:",epoch,lossfn,lr,optimizer)
    file_output.write(global_train_test_code.format(epoch,lossfn,lr,optimizer))
    file_output.write(global_train_test_code_print)
    file_output.write(global_loss_visualize.format(proj_output+"/pic/loss.jpg"))
    file_output.write(global_acc_visualize.format(proj_output+"/pic/acc.jpg"))
    file_output.close()

# 主要函数：生成代码
def gen_code(project_id):
    # 1. import code
    # 2. modol code
    # 3. train && test code
    proj = project_table.query.get(project_id)
    dataset_id = proj.project_dataset_id
    #print("Debug dataset_id:",dataset_id)
    dataset = dataset_table.query.get(int(dataset_id))
    dataset_name = dataset.dataset_name
    #print("Debug gen_code dataset_name:", dataset_name)
    #生成模型代码文件
    proj = project_table.query.get(project_id)
    proj_output = proj.project_outpath
    model_file_path = proj_output+"/model.py"
    log_output_path = proj_output+"/log/log.txt"
    loss_train_output_path = proj_output+"/log/loss_train.txt"
    loss_test_output_path = proj_output+"/log/loss_test.txt"
    acc_train_output_path = proj_output+"/log/acc_train.txt"
    acc_test_output_path = proj_output+"/log/acc_test.txt"
    
    #print("* 288 [global_node_lst] :", global_node_lst)
    if not check_structure(project_id):
        return False
    #print("* 291 [global_node_lst] :", global_node_lst)
    model_name = "myModel"
    
    ## 生成 import_code 
    import_code = global_import_packge.format(  dataset_name,log_output_path,
                                                loss_train_output_path,loss_test_output_path, 
                                                acc_train_output_path,acc_test_output_path)

    model_output = open(model_file_path,"w")
    #print(model_output)
    #print("[Debug gen_model_code] model_ouput:{}".format(model_output))
    #print("* 299 [global_node_lst] :", global_node_lst)
    model_code = gen_model_code(model_name, project_id, dataset_name, import_code)
    #print("[Debug gen_model_code]: {} - {}".format(model_file_path, model_code[0:20]))
    model_output.write(model_code)
    model_output.close()
    print("Genarate model code success!!!!\n")
    #生成训练代码文件：
    gen_train_code(project_id, dataset_name, import_code)
    print("Genarate train code success!!!!\n")

    # 生成code.py:集合model.py和train.py
    train_file_path = proj_output+"/train.py"
    code_file_path = proj_output+"/code.py"
    model_file = open(model_file_path, "r")
    train_file = open(train_file_path, "r")
    code_file = open(code_file_path, "w")
    code_file.write(model_file.read())
    code_file.write(train_file.read())
    model_file.close()
    train_file.close()
    code_file.close()
    return True


